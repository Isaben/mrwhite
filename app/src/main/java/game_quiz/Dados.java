package game_quiz;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Isaben on 20/05/2016.
 */
public class Dados {
    public static final ArrayList<String[]> elementos = new ArrayList<>(
            Arrays.asList(new String[]{"89", "227", "3"}, //actinio
                    new String[]{"13", "26.981", "13"},   //aluminio
                    new String[]{"95", "243", "10"},      //americio
                    new String[]{"51", "121.760", "15"},  //antimonio
                    new String[]{"18", "39.948", "18"},   //argonio
                    new String[]{"33", "74.921", "15"},   //arsenio
                    new String[]{"85", "210", "17"},      //astato
                    new String[]{"56", "137.327", "2"},   //bario
                    new String[]{"4", "9.012", "2"},      // berilio
                    new String[]{"97", "247", "12"},      //berquelio
                    new String[]{"83", "208", "15"},      //bismuto
                    new String[]{"5", "10.81", "13"},     //boro
                    new String[]{"35", "79.904", "17"},   //bromo
                    new String[]{"48", "112.414", "12"},  //cadmio
                    new String[]{"20", "40.078", "2"},    //calcio
                    new String[]{"98", "251", "13"},      //californio
                    new String[]{"6", "12", "14"},        //carbono
                    new String[]{"58", "140.116", "5"},   //cerio
                    new String[]{"55", "132.90", "1"},    //cesio
                    new String[]{"82", "207.2", "14"},    //chumbo
                    new String[]{"17", "35.45", "17"},    //cloro
                    new String[]{"27", "58.933", "9"},    //cobalto
                    new String[]{"29", "63.546", "11"},   //cobre
                    new String[]{"36", "83.798", "18"},   //kriptonio
                    new String[]{"24", "51.996", "6"},    //cromio
                    new String[]{"96", "247", "11"},      //curio
                    new String[]{"66", "162.500", "13"},  //disprosio
                    new String[]{"99", "252", "14"},      //einstenio
                    new String[]{"16", "32.06", "16"},    //enxofre
                    new String[]{"68", "167.259", "15"},  //erbio
                    new String[]{"21", "44.955", "3"},    //escandio
                    new String[]{"50", "118.710", "14"},  //estanho
                    new String[]{"38", "87.62", "2"},     //estroncio
                    new String[]{"63", "151.964", "10"},  //europio
                    new String[]{"100", "257", "15"},     //fermio
                    new String[]{"26", "55.845", "8"},    //ferro
                    new String[]{"9", "18.998", "17"},    //fluor
                    new String[]{"15", "30.973", "15"},   //fosforo
                    new String[]{"87", "223", "1"},       //francio
                    new String[]{"64", "157.25", "11"},   //gadolinio
                    new String[]{"31", "69.723", "13"},   //galio
                    new String[]{"32", "72.63", "14"},    //germanio
                    new String[]{"72", "178.49", "4"},    //hafnio
                    new String[]{"2", "4", "18"},         //helio
                    new String[]{"1", "1", "1"},          //hidrogenio
                    new String[]{"67", "164.93", "14"},   //holmio
                    new String[]{"49", "114.81", "13"},   //indio
                    new String[]{"53", "126.90", "17"},   //iodo
                    new String[]{"77", "192.21", "9"},    //iridio
                    new String[]{"70", "173.05", "17"},   //iterbio
                    new String[]{"39", "88.90", "3"},     //itrio
                    new String[]{"57", "138.90", "4"},    //lantanio
                    new String[]{"103", "262", "18"},     //laurencio
                    new String[]{"3", "6.94", "1"},       //litio
                    new String[]{"71", "174.96", "18"},   //lutecio
                    new String[]{"12", "24.305", "2"},    //magnesio
                    new String[]{"25", "54.938", "7"},    //manganes
                    new String[]{"101", "258", "16"},     //mendelevio
                    new String[]{"80", "200.59", "12"},   //mercurio
                    new String[]{"42", "95.95", "6"},     //molibdenio
                    new String[]{"60", "144.24", "7"},    //neodimio
                    new String[]{"10", "20.17", "18"},    //neonio
                    new String[]{"93", "237", "8"},       //neptunio
                    new String[]{"41", "92.90", "5"},     //niobio
                    new String[]{"28", "58.69", "10"},    //niquel
                    new String[]{"7", "14", "15"},        //nitrogenio
                    new String[]{"102", "259", "17"},     //nobelio
                    new String[]{"76", "190.23", "8"},    //osmio
                    new String[]{"79", "196.96", "11"},   //ouro
                    new String[]{"8", "16", "16"},        //oxigenio
                    new String[]{"46", "106.42", "10"},   //paladio
                    new String[]{"78", "195.08", "10"},   //platina
                    new String[]{"94", "244", "9"},       //plutonio
                    new String[]{"84", "209", "16"},      //polonio
                    new String[]{"19", "39.09", "1"},     //potassio
                    new String[]{"59", "140.90", "6"},    //praseodimio
                    new String[]{"47", "107.86", "11"},   //prata
                    new String[]{"61", "145", "8"},       //promecio
                    new String[]{"91", "231.03", "6"},    //protactinio
                    new String[]{"88", "226", "2"},       //radio
                    new String[]{"86", "222", "18"},      //radonio
                    new String[]{"75", "186.20", "7"},    //renio
                    new String[]{"45", "102.90", "9"},    //rodio
                    new String[]{"37", "85.46", "1"},     //rubidio
                    new String[]{"44", "101.07", "8"},    //rutenio
                    new String[]{"62", "150.36", "9"},    //samario
                    new String[]{"34", "78.970", "16"},   //selenio
                    new String[]{"14", "28.085", "14"},   //silicio
                    new String[]{"11", "22.98", "1"},     //sodio
                    new String[]{"81", "204.38", "13"},   //talio
                    new String[]{"73", "180.94", "5"},    //tantalo
                    new String[]{"43", "98", "7"},        //tecnecio
                    new String[]{"52", "127.60", "16"},   //telurio
                    new String[]{"65", "158.92", "12"},   //terbio
                    new String[]{"22", "47.867", "4"},    //titanio
                    new String[]{"90", "232.03", "5"},    //torio
                    new String[]{"69", "168.93", "16"},   //tulio
                    new String[]{"74", "183.84", "6"},    //tungstenio
                    new String[]{"92", "238.02", "7"},    //uranio
                    new String[]{"23", "50.94", "5"},     //vanadio
                    new String[]{"54", "131.29", "18"},   //xenonio
                    new String[]{"30", "65.38", "12"},    //zinco
                    new String[]{"40", "91.224", "4"})    //zirconio
    );
}
