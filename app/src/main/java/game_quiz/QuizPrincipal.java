package game_quiz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pet_engcomp.mrwhite.R;

import java.io.IOException;
import java.util.Random;

/**
 * Created by PET - Eng Comp on 19/05/2016.
 */
public class QuizPrincipal extends Activity {
    String[] Nomes;
    TextView pergunta;
    TextView numero_atomico;
    TextView massa_atomica;
    TextView periodos;
    public int corretos;
    public int pontos;
    public boolean control_pontos;
    ImageView elemento;
    Button botoes[];
    public int utilizados[];
    int corr_answ;
    int qtd_usados;
    AssetManager assets;

    public static final int QTD_PERGUNTAS = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_principal);

        init();

        updateScreen();
    }

    public void updateScreen(){
        if(corretos == QTD_PERGUNTAS) {
            final AlertDialog alerta;

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //define o titulo
            builder.setTitle("Terminou!");
            builder.setCancelable(false);

            //define a mensagem
            builder.setMessage("Você acertou de primeira "+String.valueOf(pontos)+" elementos! O que deseja fazer agora?");

            builder.setPositiveButton("Voltar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    try{
                        finish();
                    }
                    catch(Throwable i){
                    }
                }
            });

            builder.setNegativeButton("Reiniciar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            });
            init();
            updateScreen();
            builder.show();

        }
        control_pontos = true;
        pergunta.setText(getResources().getString(R.string.question, (corretos+ 1), QTD_PERGUNTAS));

        int id_elemento = randomInteger(0, 102, utilizados);
        String nome_elem = Nomes[id_elemento];
        utilizados[qtd_usados] = id_elemento;
        qtd_usados++;

        try {
            Bitmap imagem = BitmapFactory.decodeStream(assets.open("elements/" + Nomes[id_elemento]));
            elemento.setImageBitmap(imagem);
        }
        catch(IOException i){
            Log.e("Error de Insercao", "Fudeu", i);
        }

        String[] dados = Dados.elementos.get(id_elemento);
        numero_atomico.setText("Número Atômico do Elemento: " + dados[0]);
        massa_atomica.setText("Massa Atômica do Elemento: "+ dados[1]);
        periodos.setText("Grupo da Tabela do Elemento: "+ dados[2]);

        corr_answ = randomInteger(0, 3, new int[0]);
        botoes[corr_answ].setText(nome_elem.substring(0, nome_elem.length() - 4));


        for(int i = 0; i < 4; i++){
            botoes[i].setEnabled(true);
            if(i == corr_answ)
                continue;
            String texto = Nomes[randomInteger(0, 102, utilizados)];
            botoes[i].setText(texto.substring(0, texto.length()-4 ));
        }
    }

    public void buttonOneClick(View v){
        if(0 == corr_answ) {
            if(control_pontos)
                pontos++;

            corretos++;

            updateScreen();
            return;
        }
        botoes[0].setEnabled(false);
        control_pontos = false;
    }

    public void buttonTwoClick(View v){
        if(1 == corr_answ) {
            if(control_pontos)
                pontos++;

            corretos++;
            updateScreen();
            return;
        }
        control_pontos = false;
        botoes[1].setEnabled(false);
    }

    public void buttonThreeClick(View v){
        if(2 == corr_answ) {
            if(control_pontos)
                pontos++;

            corretos++;
            updateScreen();
            return;
        }
        control_pontos = false;
        botoes[2].setEnabled(false);
    }

    public void buttonFourClick(View v){
        if(3 == corr_answ) {
            if(control_pontos)
                pontos++;

            corretos++;
            updateScreen();
            return;
        }
        control_pontos = false;
        botoes[3].setEnabled(false);
    }

    public int randomInteger(int min, int max, int[] used) {

        Random rand = new Random();

        // nextInt excludes the top value so we have to add 1 to include the top value

        int randomNum = rand.nextInt((max - min) + 1) + min;
        // exclude possiveis repeticoes
        for(int i = 0; i < used.length; i++) {
            if (randomNum == used[i]) {
                randomNum = rand.nextInt((max - min) + 1) + min;
                i = 0;
            }
        }

        return randomNum;
    }

    public void init(){
        assets = this.getAssets();
        corretos = 0;
        pontos = 0;
        pergunta = (TextView) findViewById(R.id.pergunta);
        elemento = (ImageView) findViewById(R.id.elemento);
        utilizados = new int[11];
        botoes = new Button[4];
        corr_answ = 0;
        botoes[0] = (Button) findViewById(R.id.botao1);
        botoes[1] = (Button) findViewById(R.id.botao2);
        botoes[2] = (Button) findViewById(R.id.botao3);
        botoes[3] = (Button) findViewById(R.id.botao4);
        qtd_usados = 0;

        numero_atomico = (TextView) findViewById(R.id.numero_atomico);
        massa_atomica = (TextView) findViewById(R.id.massa_atomica);
        periodos = (TextView) findViewById(R.id.periodos);

        try{
            Nomes = assets.list("elements");

        }
        catch (IOException i){
            Log.e("Pegar Imagem", "Deu erro", i);
        }
    }
}

