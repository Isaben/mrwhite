package com.example.pet_engcomp.mrwhite;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.IOException;

import game_cannon.ViewCanhao;

public class TelaPrincipal extends Activity {
    String[] Nomes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);
        AssetManager assets = this.getAssets();
        try{
            Nomes = assets.list("");

        }
        catch (IOException i){
            Log.e("yolo", "fudeu", i);
        }

    }

    public void startQuiz(View v){
        Intent intent = new Intent(TelaPrincipal.this, game_quiz.QuizPrincipal.class);
        startActivity(intent);
    }

    public void startCannon(View v){
        Intent intent = new Intent(TelaPrincipal.this, game_cannon.MainActivity.class);
        startActivity(intent);
    }
}
