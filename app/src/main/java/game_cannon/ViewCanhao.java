package game_cannon;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


import com.example.pet_engcomp.mrwhite.R;

import java.util.Random;

/**
 * Created by Isaben on 18/05/2016.
 */
public class ViewCanhao extends SurfaceView implements SurfaceHolder.Callback {
    private static final String TAG = "ViewCanhao";
    private CannonThread cannonThread;
    private Activity activity;
    private boolean dialogIsDisplayed = false;
    public static final int TARGET_PIECES = 7;
    public static final int MISS_PENALTY = 2;
    public static final int HIT_REWARD = 3;
    private boolean gameOver;
    private double timeLeft;
    private int shotsFired;
    private double totalElapsedTime;
    private Line blocker;
    private int blockerDistance;
    private int blockerBeginning;
    private int blockerEnd;
    private int initialBlockerVelocity;
    private float blockerVelocity;
    private Line target;
    private int targetDistance;
    private int targetBeginning;
    private double pieceLength;
    private int targetEnd;
    private int initialTargetVelocity;
    private float targetVelocity;
    private int lineWidth;
    private boolean[] hitStates;

    private Point cannonball;
    private int cannonballVelocityX;
    private int cannonballVelocityY;
    private boolean cannonballOnScreen;
    private int cannonballRadius;
    private int cannonballSpeed;
    private int cannonBaseRadius;
    private int cannonLength;
    private Point barrelEnd;
    private int screenWidth;
    private int screenHeight;

    private Paint textPaint;
    private Paint cannonballPaint;
    private Paint cannonPaint;
    private Paint blockerPaint;
    private Paint targetPaint;
    private Paint backgroundPaint;
    private String[] elementos;
    private String answer;
    private String tentativa;
    private String[] dataget;
    private int used[];
    private int qtd_usado;

    public ViewCanhao(Context context, AttributeSet attrs) {
        super(context, attrs);
        activity = (Activity) context;
        getHolder().addCallback(this);
        blocker = new Line();
        target = new Line();
        cannonball = new Point();
        hitStates = new boolean[TARGET_PIECES];



        textPaint = new Paint();
        cannonPaint = new Paint();
        cannonballPaint = new Paint();
        blockerPaint = new Paint();
        targetPaint = new Paint();
        backgroundPaint = new Paint();
        elementos = new String[7];

    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenWidth = w; screenHeight = h;
        cannonBaseRadius = h / 18;
        cannonLength = w / 8;
        cannonballRadius = w / 36;
        cannonballSpeed = w * 3 / 2;
        lineWidth = w / 24;
        blockerDistance = w * 5 / 8;
        blockerBeginning = h / 8;
        blockerEnd = h * 3 / 8;
        initialBlockerVelocity = h / 2;
        blocker.start = new Point(blockerDistance, blockerBeginning);
        blocker.end = new Point(blockerDistance, blockerEnd);
        targetDistance = w * 7 / 8;
        targetBeginning = h / 8;
        targetEnd = h * 7 / 8;
        pieceLength = (targetEnd - targetBeginning) / TARGET_PIECES;
        initialTargetVelocity = -h / 4;
        target.start = new Point(targetDistance, targetBeginning);
        target.end = new Point(targetDistance, targetEnd);
        barrelEnd = new Point(cannonLength, h / 2);
        textPaint.setTextSize(w / 20);
        textPaint.setAntiAlias(true);

        cannonPaint.setStrokeWidth(lineWidth * 1.5f);
        blockerPaint.setStrokeWidth(lineWidth);
        targetPaint.setStrokeWidth(lineWidth);
        backgroundPaint.setColor(Color.WHITE);
        newGame();
    }
    public void newGame() {
        used = new int[3];
        answer = "";
        tentativa = "";
        qtd_usado = 0;
        for (int i = 0; i < TARGET_PIECES; i++)
            hitStates[i] = false;

        blockerVelocity = initialBlockerVelocity;
        targetVelocity = initialTargetVelocity;
        timeLeft = 10;
        cannonballOnScreen = false;
        shotsFired = 0;
        totalElapsedTime = 0.0;
        blocker.start.set(blockerDistance, blockerBeginning);
        blocker.end.set(blockerDistance, blockerEnd);
        target.start.set(targetDistance, targetBeginning);
        target.end.set(targetDistance, targetEnd);
        if (gameOver) {
            gameOver = false;
            cannonThread = new CannonThread(getHolder());
            cannonThread.start();
        }
        dataget = DadosCanhao.elementos.get(randomInteger(0, 21, new int[0]));
        for(int i = 0; i < dataget.length; i++) {
            answer += dataget[i];
            int temp = getIndice(used);
            elementos[temp] = dataget[i];
            used[qtd_usado] = temp;
            qtd_usado++;
        }
        elementos[getIndice(used)] = DadosCanhao.errados[randomInteger(0, 16, new int[0])];
    }
    private void updatePositions(double elapsedTimeMS) {
        double interval = elapsedTimeMS / 1000.0;
        if (cannonballOnScreen) {
            cannonball.x += interval * cannonballVelocityX;
            cannonball.y += interval * cannonballVelocityY;
            if (cannonball.x + cannonballRadius > blockerDistance && cannonball.x - cannonballRadius < blockerDistance && cannonball.y + cannonballRadius > blocker.start.y && cannonball.y - cannonballRadius < blocker.end.y) {
                cannonballVelocityX *= -1;
                timeLeft -= MISS_PENALTY;

            }
            else if (cannonball.x + cannonballRadius > screenWidth || cannonball.x - cannonballRadius < 0) {
                cannonballOnScreen = false;
            }
            else if (cannonball.y + cannonballRadius > screenHeight || cannonball.y - cannonballRadius < 0) {
                cannonballOnScreen = false;
            }
            else if (cannonball.x + cannonballRadius > targetDistance && cannonball.x - cannonballRadius < targetDistance && cannonball.y + cannonballRadius > target.start.y && cannonball.y - cannonballRadius < target.end.y) {
                int section = (int) ((cannonball.y - target.start.y) / pieceLength);
                if ((section >= 0 && section < TARGET_PIECES) && !hitStates[section]) {
                    if(section % 2 == 1){
                        tentativa = tentativa + elementos[section];
                    }
                    hitStates[section] = true;
                    cannonballOnScreen = false;
                    timeLeft += HIT_REWARD;

                    if (tentativa.equals(answer)) {
                        cannonThread.setRunning(false);
                        showGameOverDialog(R.string.win);
                        gameOver = true;
                    }
                    if(hitStates[1] && hitStates[3] && hitStates[5]){
                        gameOver = true;
                        cannonThread.setRunning(false);
                        showGameOverDialog(R.string.lose);
                    }
                }
            }
        }
        double blockerUpdate = interval * blockerVelocity;
        blocker.start.y += blockerUpdate;
        blocker.end.y += blockerUpdate;
        double targetUpdate = interval * targetVelocity;
        target.start.y += targetUpdate;
        target.end.y += targetUpdate;
        if (blocker.start.y < 0 || blocker.end.y > screenHeight)
            blockerVelocity *= -1;
        if (target.start.y < 0 || target.end.y > screenHeight)
            targetVelocity *= -1;
        timeLeft -= interval;
        if (timeLeft <= 0.0) {
            timeLeft = 0.0;
            gameOver = true;
            cannonThread.setRunning(false);
            showGameOverDialog(R.string.lose);
        }
    }
    public void fireCannonball(MotionEvent event) {
        if (cannonballOnScreen)
            return;
        double angle = alignCannon(event);
        cannonball.x = cannonballRadius;
        cannonball.y = screenHeight / 2;
        cannonballVelocityX = (int) (cannonballSpeed * Math.sin(angle));
        cannonballVelocityY = (int) (-cannonballSpeed * Math.cos(angle));
        cannonballOnScreen = true;
        ++shotsFired;

    }
    public double alignCannon(MotionEvent event) {
        Point touchPoint = new Point((int) event.getX(), (int) event.getY());
        double centerMinusY = (screenHeight / 2 - touchPoint.y);
        double angle = 0;
        if (centerMinusY != 0) angle = Math.atan((double)
                touchPoint.x / centerMinusY);
        if (touchPoint.y > screenHeight / 2)
            angle += Math.PI;
        barrelEnd.x = (int) (cannonLength * Math.sin(angle));
        barrelEnd.y = (int) (-cannonLength * Math.cos(angle) + screenHeight / 2);
        return angle;
    }
    public void drawGameElements(Canvas canvas) {
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), backgroundPaint);
        canvas.drawText(getResources().getString( R.string.time_remaining_format, timeLeft), 30, 50, textPaint);
        canvas.drawText("Você deve formar: "+answer, 30, 100, textPaint);
        if (cannonballOnScreen)
            canvas.drawCircle(cannonball.x, cannonball.y, cannonballRadius, cannonballPaint);
        canvas.drawLine(0, screenHeight / 2, barrelEnd.x, barrelEnd.y, cannonPaint);
        canvas.drawCircle(0, (int) screenHeight / 2, (int) cannonBaseRadius, cannonPaint);
        canvas.drawLine(blocker.start.x, blocker.start.y, blocker.end.x, blocker.end.y, blockerPaint);
        Point currentPoint = new Point();
        currentPoint.x = target.start.x;
        currentPoint.y = target.start.y;
        for (int i = 0; i < TARGET_PIECES; i++) {
            if (!hitStates[i]) {
                if (i % 2 != 0) {
                    targetPaint.setColor(Color.BLUE);
                    canvas.drawText(elementos[i], currentPoint.x-100, currentPoint.y+60, textPaint);

                }
                else
                    targetPaint.setColor(Color.YELLOW);
                canvas.drawLine(currentPoint.x, currentPoint.y, target.end.x, (int) (currentPoint.y + pieceLength), targetPaint);
            }
            currentPoint.y += pieceLength;
        }
    }
    private void showGameOverDialog(final int messageId)
    {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        dialogBuilder.setTitle(getResources().getString(messageId));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setMessage(" Resposta: "+answer+" Você formou: "+tentativa);
        dialogBuilder.setPositiveButton(R.string.reset_game, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                dialogIsDisplayed = false; newGame(); }
        });
        dialogBuilder.setNegativeButton("Voltar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                try{
                    activity.finish();
                }
                catch(Throwable i){
                    Log.e("Fudeu", "Deu Pau", i);
                }
            }
        });
        activity.runOnUiThread(
                new Runnable() {
                    public void run() {
                        dialogIsDisplayed = true;
                        dialogBuilder.show();
                    }
                }
        );
    }
    public void stopGame() {
        if (cannonThread != null)
            cannonThread.setRunning(false);
    }
    public void releaseResources() {

    }
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!dialogIsDisplayed) {
            cannonThread = new CannonThread(holder);
            cannonThread.setRunning(true);
            cannonThread.start();
        }
    }
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        cannonThread.setRunning(false);
        while (retry) {
            try {
                cannonThread.join();
                retry = false;
            }
            catch (InterruptedException e) {
                Log.e(TAG, "Thread interrupted", e);
            }
        }
    }
    public boolean onTouchEvent(MotionEvent e) {
        int action = e.getAction();
        if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE) {
            fireCannonball(e);
        }
        return true;
    }
    private class CannonThread extends Thread {
        private SurfaceHolder surfaceHolder;
        private boolean threadIsRunning = true;
        public CannonThread(SurfaceHolder holder) {
            surfaceHolder = holder;
            setName("CannonThread");
        }
        public void setRunning(boolean running) {
            threadIsRunning = running;
        }
        @Override
        public void run() {
            Canvas canvas = null;
            long previousFrameTime = System.currentTimeMillis();
            while (threadIsRunning) {
                try {
                    canvas = surfaceHolder.lockCanvas(null);
                    synchronized(surfaceHolder) {
                        long currentTime = System.currentTimeMillis();
                        double elapsedTimeMS = currentTime - previousFrameTime;
                        totalElapsedTime += elapsedTimeMS / 1000.0;
                        updatePositions(elapsedTimeMS);
                        drawGameElements(canvas);
                        previousFrameTime = currentTime;
                    }
                }
                finally {
                    if (canvas != null)
                        surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    public int randomInteger(int min, int max, int[] used) {

        Random rand = new Random();

        // nextInt excludes the top value so we have to add 1 to include the top value

        int randomNum = rand.nextInt((max - min) + 1) + min;
        // exclude possiveis repeticoes
        for(int i = 0; i < used.length; i++) {
            if (randomNum == used[i]) {
                randomNum = rand.nextInt((max - min) + 1) + min;
                i = 0;
            }
        }

        return randomNum;
    }
    public int getIndice(int[] used){
        Random rand = new Random();

        // nextInt excludes the top value so we have to add 1 to include the top value
        boolean temp = true;

        while(true){
            int randomNum = rand.nextInt((5 - 1) + 1) + 1;
            if(randomNum == 1 || randomNum == 3 || randomNum == 5){
                for(int i = 0; i < qtd_usado; i++){
                    if(randomNum == used[i])
                        temp = false;
                }
                if(temp)
                    return randomNum;
            }
            temp = true;
        }
    }
}