package game_cannon;


import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Isaben on 21/05/2016.
 */
public class DadosCanhao {
    public static final ArrayList<String[]> elementos = new ArrayList<>(
            Arrays.asList(new String[]{"Na", "Cl"},
                    new String[]{"H", "Cl"},
                    new String[]{"H","C","N"},
                    new String[]{"Ca","O"},
                    new String[]{"Mg","O"},
                    new String[]{"Na","O","H"},
                    new String[]{"K","O","H"},
                    new String[]{"Na","I"},
                    new String[]{"K","I"},
                    new String[]{"C","O"},
                    new String[]{"Fe", "O"},
                    new String[]{"Cu", "O"},
                    new String[]{"K", "Cl"},
                    new String[]{"Ba", "S"},
                    new String[]{"Hg", "O"},
                    new String[]{"Na", "F"},
                    new String[]{"H", "Cl", "O"},
                    new String[]{"O", "H"},
                    new String[]{"N", "H"},
                    new String[]{"Zn", "S"},
                    new String[]{"H", "F"},
                    new String[]{"Ba", "Cl"})) ;

    public static final String errados[] = {"La", "Ce", "Pm", "Th", "Pa", "Sm", "Gd", "Ga", "Sr", "Mt", "Rf"
                                            , "Nb", "Ru", "Pt", "No", "Ac", "Fr"};
}
